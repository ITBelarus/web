# webproject.com
In docker
1. Setup docker && docker-compouse && git && certbot
2. Clone project
3. Make chages .ENV
4. Make changes in configuration.php
5. Put DB dump /mysql-5.7/dump
6. docker-compose build && docker-compose up -d
7. docker exec -it mysql mysql --user=root --password=secret (create user and db)
8. docker exec -i mysql mysql --user=root --password=secret siteDB < /dump/siteDB.sql
9. Make ssl docker compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d example.org
10. public $force_ssl = '2'; in configuration.php
