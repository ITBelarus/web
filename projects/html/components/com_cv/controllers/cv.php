<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


class CVControllerCV extends JControllerForm
{
    public function submit()
    {
        /** @var \Joomla\CMS\Application\SiteApplication $app */
        $app = JFactory::getApplication();

        /** @var CVModelCV $model */
        $model = $this->getModel('cv');

        /** @var \Joomla\CMS\Input\Input $input */
        $input = $this->input;

        $data = $input->post->get('jform', array(), 'array');
        $data['cv_file'] = $input->files->getPath('cv_file');

        $successUrl = isset($data['success_url']) ? $data['success_url'] : '/';
        $errorUrl = isset($data['error_url']) ? $data['error_url'] : '/';

        $isSent = $model->submit($data);

        if ($isSent) {
            $this->setRedirect($successUrl, $msg = 'OK', $msgType = 'message');
        } else {
            JLog::add('Unable to send CV from the site: '. json_encode($data), JLog::ERROR, 'jerror');
            $this->setRedirect($errorUrl, $msg = 'OK', $msgType = 'message');
        }
    }
}