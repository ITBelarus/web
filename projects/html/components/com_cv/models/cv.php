<?php
/**
 * @package    cv
 *
 * @author     zmiter <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\MVC\Model\BaseDatabaseModel;

defined('_JEXEC') or die;

class CvModelCv extends BaseDatabaseModel
{
	protected $errors = array();

    public function submit($data)
    {
        $data = $this->processInput($data);

        if (empty($this->errors)) {
            $body = $this->renderBody($data);

            return $this->sendEmail($data['to'], $data['subject'], $body, $data['cv_file']);
        }

        return false;
    }

    protected function processInput($data)
    {
        if (!isset($data['full_name'])) {
            $this->errors[] = 'invalid full_name';
        }

        if (!isset($data['email'])) {
            $this->errors[] = 'invalid email';
        }

        if (!isset($data['cv_file']) || (isset($data['cv_file']) && $data['cv_file']['error'] > 0)) {
            $this->errors[] = 'invalid cv_file';
        }
        if (!isset($data['vac_key'])) {
            $this->errors[] = 'invalid vac_key';
        }

        if (!isset($data['phone'])) {
            $data['phone'] = '';
        }

        if (!isset($data['message'])) {
            $data['message'] = '';
        }

        if (!isset($data['to'])) {
            $data['to'] = 'info@teqniksoft.com';
        }

        if (!isset($data['subject'])) {
            $data['subject'] = 'New CV';
        }

        return $data;
    }

    public function getErrorMsg()
    {
        return $this->errors;
    }

    protected function renderBody($data)
    {
        return <<<HTML
<p>Vacancy Job Key: <span>{$data['vac_key']}</span></p>
<p>Full Name: <span>{$data['full_name']}</span></p>
<p>Email: <span>{$data['email']}</span></p>
<p>Phone: <span>{$data['phone']}</span></p>
<p>Message: <span>{$data['message']}</span></p>
HTML;
    }

    protected function sendEmail($to, $subject, $body, $cv)
    {
        /** @var \Joomla\CMS\Mail\Mail $mail */
        /** @var \Joomla\CMS\Application\SiteApplication $app */

        $app = JFactory::getApplication();

        $mailfrom = $app->get('mailfrom');
        $fromname = $app->get('fromname');

        try {
            $mail = JFactory::getMailer();
            $mail->addRecipient($to);
            $mail->setSender(array($mailfrom, $fromname));
            $mail->setSubject($subject);
            $mail->setBody($body);
            $mail->isHtml(true);
            $mail->addAttachment($cv['tmp_name'], $cv['name']);

            $result = $mail->Send();

            if ($result instanceof \JException) {
                return false;
            }

            $sent = $result === true;
            //$sent = is_bool($result);
        } catch (\Exception $e) {
            $sent = false;
        }

        return $sent;
    }
}
