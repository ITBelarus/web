<?php
/**
 * @package    cv
 *
 * @author     zmiter <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\MVC\Model\ListModel;

defined('_JEXEC') or die;

/**
 * Cv
 *
 * @package  cv
 * @since    1.0
 */
class CvModelCv extends ListModel
{
}
