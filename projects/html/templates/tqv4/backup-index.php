<?php
defined('_JEXEC') or die;
?><!doctype html>
<html lang="<?php echo $this->language; ?>" class="lang-<?php echo $this->language; ?>">
<head>
	<jdoc:include type="head" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!--
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,400italic,600,700,300" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,400italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
-->
	<link rel="stylesheet" href="<?php echo($this->baseurl . '/templates/' . $this->template) ?>/css/libs.min.css">
	<link rel="stylesheet" href="<?php echo($this->baseurl . '/templates/' . $this->template) ?>/css/style.min.css">
	<script defer src="<?php echo($this->baseurl . '/templates/' . $this->template) ?>/js/libs.min.js"></script>
	<script defer src="<?php echo($this->baseurl . '/templates/' . $this->template) ?>/js/scripts.min.js"></script>
	<link rel="icon" type="image/x-icon" href="<?php echo $this->baseurl; ?>/favicon.ico" />
	
	<?php echo JHtml::_('behavior.formvalidator');?>
	
</head>
<body>

<script type="text/javascript" src="https://secure.hiss3lark.com/js/175946.js" ></script>
<noscript><img alt="" src="https://secure.hiss3lark.com/175946.png" style="display:none;" /></noscript>

<header class="header">
	<div class="header-cont">
		<a href="/" class="logo"><img src="<?php echo($this->baseurl . '/templates/' . $this->template) ?>/img/logo.png" alt=""></a>
		<div class="header-menu">
			<jdoc:include type="modules" name="main-nav" />
<!--			<ul>-->
<!--
				<li>
					<a href="#">OUR EXPERTISE</a>
					<div class="hover-menu">
						<a href="#">Web & Desktop Applications</a>
						<a href="#">Embedded Software</a>
						<a href="#">Mobile Solutions</a>
						<a href="#">User Experience</a>
						<a href="#">Data Science</a>
					</div>
				</li>
				<li><a href="">CASE STUDIES</a></li>
				<li><a href="">ABOUT US</a></li>
				<li><a href="">Careers</a></li>
				<li><a href="">Contact</a></li>
-->
<!--			</ul>-->
		</div>
		<a class="show-mobile-menu" href="#"><span></span></a>
	</div>
</header>

<div class="header-menu-mobile">
	<jdoc:include type="modules" name="main-nav" />
<!--	<ul>-->
<!--
		<li><a href="#">OUR EXPERTISE</a></li>
		<li><a href="#">CASE STUDIES</a></li>
		<li><a href="#">ABOUT US</a></li>
		<li><a href="#">Careers</a></li>
		<li><a href="#">Contact</a></li>
-->
<!--	</ul>-->
	<a class="hide-mobile-menu" href="#" ></a>
</div>

<div class="wrapper">

	<div class="content">
		<jdoc:include type="component" />	
		
	</div>
	
	
	
</div>



<!--
<div class="container">
	<div class="page-wrap">
		<header class="topnav">
			<div class="topnav-mobile">
				<div class="topnav-inner">
					<button type="button" class="topnav-toggle" title="Show menu">
						<i class="icon-menu"></i>
					</button>
					<div class="lang-switcher">
						<jdoc:include type="modules" name="select-language" />
					</div>
					<a href="<?php echo $this->baseurl; ?>" class="logo">
						<div class="visible-bright"><img src="<?php echo $this->baseurl; ?>/images/logo-bright.png" alt="Teqniksoft"></div>
						<div class="visible-dark"><img src="<?php echo $this->baseurl; ?>/images/logo-dark.png" alt="Teqniksoft"></div>
					</a>
				</div>
			</div>
			<div class="topnav-main">
				<div class="topnav-inner">
					<button type="button" class="topnav-toggle" title="Hide menu">
						<i class="icon-clear"></i>
					</button>
					<div class="lang-switcher">
						<jdoc:include type="modules" name="select-language" />
					</div>
					<a href="<?php echo $this->baseurl; ?>" class="logo">
						<div class="visible-bright"><img src="<?php echo $this->baseurl; ?>/images/logo-bright.png" alt="Teqniksoft"></div>
						<div class="visible-dark"><img src="<?php echo $this->baseurl; ?>/images/logo-dark.png" alt="Teqniksoft"></div>
					</a>
					<div class="main-nav"><jdoc:include type="modules" name="main-nav" /></div>
				</div>
			</div>
		</header>
		<div class="clearfix"></div>
	</div>
</div>
-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104887073-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>