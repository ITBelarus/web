<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');

//$validatePhone = <<<JS
//	jQuery(document).ready(function(){
//		document.formvalidator.setHandler('phone', function(value) {
//			const regex=/^\+?([0-9]{2,3})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
//			let result = regex.test(value);
//			return result;
//		});
//	});
//JS;
//$document = JFactory::getDocument();
//$document->addScriptDeclaration($validatePhone);

if (isset($this->error)) : ?>
	<div class="contact-error">
		<?php echo $this->error; ?>
	</div>
<?php endif; ?>

<div class="contact-form">
	<form id="contact-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate">
		<fieldset>

			<h5>Subject</h5>

			<div class="form-group">
				<?php echo str_replace('<input type="text"', '<input type="text" class="form-control"', $this->form->getInput('contact_subject')); ?>
			</div>

			<h5>Contact Details</h5>

			<div class="form-group">
				<?php echo str_replace('<input type="text"', '<input type="text" placeholder="Full Name" class="form-control"', $this->form->getInput('contact_name')); ?>
			</div>
			<div class="form-group">
				<?php echo str_replace('class="validate-email', 'placeholder="Email Address" class="validate-email form-control', $this->form->getInput('contact_email')); ?>
			</div>

			<div class="form-group">
                <?php echo str_replace('class="required', 'class="validate-phone form-control required', $this->form->getInput('phone', 'com_fields')); ?>
			</div>

			<h5>Message</h5>

			<div class="form-group">
				<?php echo str_replace('<textarea ', '<textarea class="form-control"', $this->form->getInput('contact_message')); ?>
			</div>
			<?php /*if ($this->params->get('show_email_copy')) { ?>
				<div class="form-group">

					<?php echo $this->form->getInput('contact_email_copy'); ?>
					<?php echo $this->form->getLabel('contact_email_copy'); ?>
				</div>
			<?php } */?>
			<?php //Dynamically load any additional fields from plugins. ?>
			<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
				<?php if ($fieldset->name != 'contact' && $fieldset->name != 'fields-0'):?>
					<?php $fields = $this->form->getFieldset($fieldset->name);?>
					<?php foreach ($fields as $field) : ?>
						<div class="form-group">
							<?php if ($field->hidden) : ?>
								<?php echo $field->input;?>
							<?php else:?>
								<?php echo $field->label; ?>
								<?php if (!$field->required && $field->type != "Spacer") : ?>
									<span class="optional"><?php echo JText::_('COM_CONTACT_OPTIONAL');?></span>
								<?php endif; ?>
								<?php echo $field->input;?>
							<?php endif;?>
						</div>
					<?php endforeach;?>
				<?php endif ?>
			<?php endforeach;?>
			<div class="form-actions">
				<button class="btn btn-action with-rarr validate" type="submit">
					<?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?><span class="rarr"></span>
				</button>
				<input type="hidden" name="option" value="com_contact" />
				<input type="hidden" name="task" value="contact.submit" />
				<input type="hidden" name="return" value="<?php echo $this->return_page;?>" />
				<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
				<?php echo JHtml::_('form.token'); ?>
			</div>
		</fieldset>
	</form>
</div>