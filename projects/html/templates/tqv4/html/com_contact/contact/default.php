<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$cparams = JComponentHelper::getParams('com_media');

jimport('joomla.html.html.bootstrap');
?>
<div class="contact<?php echo $this->pageclass_sfx?>" itemscope itemtype="http://schema.org/Person">
	
	<div class="contact-bg">
		<h2>contact us</h2>
		<div class="clearfix">
			<?php echo $this->loadTemplate('form'); ?>			
		</div>		
	</div>
	<div class="cb-bottom">
		<?php echo $this->contact->misc; ?>
	</div>
</div>

<footer class="footer">
	<div class="custom">
		<div class="footer-cont">
          <div class="promo-text">
            <h5>Learn more about our expertise</h5>
            <p>Follow the link below to read more about our offerings before contacting us.</p>
          </div>
          <div class="footer-btn">
            <a href="/services/web-desktop" class="btn btn-action">Our Expertise</a>
          </div>
          <div class="copyright">© 2018 Teqniksoft, LLC. All rights reserved. Terms of Use.  Privacy and Security Statement.</div>
          <div class="social">
            <a target="_blank" href="https://www.facebook.com/teqniksoft"><span class="icon-facebook"></span></a>
            <a target="_blank" href="https://twitter.com/teqniksoft"><span class="icon-twitter"></span></a>
            <a target="_blank" href="https://www.instagram.com/teqniksoft/"><span class="icon-instagram"></span></a>
            <a target="_blank" href="https://plus.google.com/111956504439391330781/about?hl=en"><span class="icon-gplus"></span></a>
            <a target="_blank" href="https://www.youtube.com/channel/UChAIIcL4W4QLXUDW873veHw/feed"><span class="icon-youtube"></span></a>
            <a target="_blank" href="https://www.linkedin.com/company/teqniksoft"><span class="icon-linkedin"></span></a>
          </div>
    </div>

    </div>
</footer>	
