<?php
    //Connect to local Redis service
   $redis = new Redis();
   $redis->connect('redis', 6379);
   echo "Connection to server sucessfully";
    //Check if the service is running
   echo "Server is running: "+ $redis->ping();

/**
 * @license MIT License
 * @copyright maartendekeizer
 */


class dataCache {

        function setMemData($key, $var, $flag = false, $expire = 36000) {
                global $memcache;
                if (!$memcache) {
                        $memcache = new Memcache;
                        $memcache->connect('localhost', 11211) or die("Could not connect");
                }
                if ($result = $memcache->set($key, $var, $flag, time() + $expire)) {
                        return TRUE;
                } else {
                        return FALSE;
                }
        }

        function getMemData($key) {
                global $memcache;
                if (!$memcache) {
                        $memcache = new Memcache;
                        $memcache->connect('memcache', 11211) or die("Could not connect");
                }

                if ($data = $memcache->get($key)) {
                        return $data;
                } else {
                        return FALSE;
                }
        }

        function delMemData($key) {
                global $memcache;
                if (!$memcache) {
                        $memcache = new Memcache;
                        $memcache->connect('localhost', 11211) or die("Could not connect");
                }

                if ($result = $memcache->delete($key)) {
                        return TRUE;
                } else {
                        return FALSE;
                }
        }
}

?>